# Introduction

This repository contains the scripts detailed in the publication:


**Donnellan, SC., Catalano, SR., Pederson, S., Mitchell, K., Suhendran, A., Price, L., Doughty, P. and Richards, SJ. (2021). Revision of the _Litoria watjulumensis_ (Anura: Hylidae) group from the Australian monsoonal tropics, including the resurrection of _L. spaldingi_. Zootaxa 4933, 211-240**


If you use these scripts (or modified versions) in your work, then please cite the publication.


#Contents
**model_fit.pdf** - Analysis of difference in size sexual dimorphism between two _Litoria_ populations with embedded R script






